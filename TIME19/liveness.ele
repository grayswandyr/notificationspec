/*
This file  is an abstract specification of the notification system 
for proving the liveness property.
*/

/*
succ is not constant anymore and not a function either, it is now a predicate/relation;
to replace the equality the predicate equiv is necessary
*/
sig Process {
	var succ: set Process,
	var equiv: set Process,
}

//notified represent the set of notified processes
var sig notified in Process {}

//Skolemized constants, skolem_c may be equal to skolem_d
one sig skolem_c in Process {}
one sig skolem_d in Process {}

/*
Abstraction of the equality, two processes are equivalent at some instant 
if they satisfy the same properties
*/
fact AbEquiv {
	always all p1: Process, p2: Process, p3: Process {
    p1 in p1.equiv
    (p2 in p1.equiv and p3 in p2.equiv) => p3 in p1.equiv 
    p2 in p1.equiv => p1 in p2.equiv
    p2 in p1.equiv => (p2 in notified <=> p1 in notified)
    p2 in p1.equiv => (p3 in p2.succ <=> p3 in p1.succ)
    p2 in p1.equiv => (p1 in p3.succ <=> p2 in p3.succ)
  }
}

/*
Temporal property of finite ring, instantiated for skolem_c and skolem_d.
The orgininal property states that if notified processes send a message through the ring
then any Process eventually becomes notified, it is true because of the hypothesis
that some process is notified at the initial state.
*/
fact FinRing{
  (always (skolem_c in notified
	    => (some y: Process | eventually (y in skolem_c.succ and y in notified))))
  => eventually skolem_d in notified
}

//Abstraction of Send
pred Send [p: Process] {
  some px: p.succ {
    p in notified => px in notified' else Same[px]
    Same_inst[p, px]
    px in p.succ'
  }
}

// The state of a process ps remains the same
pred Same [ps: Process] { ps in notified <=> ps in notified' }

// Instantiations of Same
pred Same_inst[p: Process, px: Process] { 
  (p in skolem_c.equiv or px in skolem_c.equiv or (skolem_c in notified <=> skolem_c in notified'))
  p in skolem_c.equiv <=> p in skolem_c.equiv'
  px in skolem_c.equiv <=> px in skolem_c.equiv'
  Same[p]
}

//There is always some process sending a message
fact Trans { always some p: Process | Send[p] }

//The fact that any notified process sends a message, but instantiated with skolem_c
fact Progress {
  always (skolem_c in notified => eventually Send[skolem_c])
}

// Consistent property: the specification, without further constraint, has a
// finite model
pred Consistent {}

//The skolemized liveness property
assert SkolemLiveness { eventually skolem_d in notified }

run Consistent
check SkolemLiveness for exactly 6 Process

