/*
This file  is an abstract specification of the notification system 
for proving the safety property.
*/

/*
succ is not constant anymore and not a function either, it is now a relation/predicate;
to replace the equality the predicate equiv is necessary
*/
sig Process {
  var succ: set Process,
  var equiv: set Process,
  var received: set Message
}

sig Message { 
}

// Skolemized constants
one sig skolem_p in Process {}
one sig skolem_m in Message {}

/*
Abstraction of the equality, two processes are equivalent at some instant 
if they satisfy the same properties
*/
fact AbEquiv {
  always all p1, p2, p3: Process {
    p1 in p1.equiv // reflexive
    p2 in p1.equiv => p1 in p2.equiv // symmetric
    (p2 in p1.equiv and p3 in p2.equiv) => p3 in p1.equiv // transitive
    p2 in p1.equiv => (p3 in p2.succ <=> p3 in p1.succ)
    p2 in p1.equiv => (p1 in p3.succ <=> p2 in p3.succ)
   }
  always all p1, p2: Process, m: Message {
    p2 in p1.equiv => (m in p2.received <=> m in p1.received)
  }
}

// Abstraction of Send
pred Send [p: Process, m: Message] {
  some px: p.succ {
    m in p.received => (m in px.received' and skolem_m in px.received') else Same[px,m]
    px in p.succ'
    Same_inst[p, px, m]
  }
}

// The state of a process ps remains the same
pred Same [ps: Process, m: Message] { 
	m in ps.received <=> m in ps.received'
	skolem_m in ps.received <=> skolem_m in ps.received'
}

// Instantiations of Same
pred Same_inst[p: Process, px: Process, m: Message] { 
  (px in skolem_p.equiv or (skolem_m in skolem_p.received <=> skolem_m in skolem_p.received'))
  p in skolem_p.equiv <=> p in skolem_p.equiv'
  px in skolem_p.equiv <=> px in skolem_p.equiv'
  Same[p,m]
}

// There is always some process sending a message
fact Trans { always some p: Process, m: Message | Send[p,m] }

// Consistent property: the specification, without further constraint, has a
// finite model
pred Consistent {}

// The skolemized safety property
assert SkolemSafety { always (skolem_m in skolem_p.received => skolem_m in skolem_p.received') }

run Consistent 
check SkolemSafety for exactly 5 Process, 3 Message

