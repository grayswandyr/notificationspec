# Toy Notification System Specification

Example Electrum files illustrating the Bounded Domain Property.

## Electrum crash course:

Statements:
- `sig` (signature) introduces a set
- fields inside signatures stand for relations
- the value of a `var` field or sig may may vary at any instant, while it is constant if non-`var`
- `pred` introduces a reusable, parameterized formula
- `fact` introduces a formula taken to be true
- `assert` introduces a formula that will have to be checked

Commands:
- `for N Process` means: for models with *up to* N processes
- `for exactly N Process` means: for models *exactly* N processes 
- `run` is a command asking to find an example
- `check` is a command asking to check that there are no counterexamples
- unless the selected Solver (in Options menu) is NuSMV or nuXmv, Electrum Analyzer performs bounded model-checking on traces featuring up to 10 (by default) distinct states. With NuSMV or nuXmv, the verification uses complete model-checking.


## Electrum Analyzer

The Electrum Analyzer can be downloaded from <http://haslab.github.io/Electrum/>.

(C) ONERA DTIS 2019-2020.
